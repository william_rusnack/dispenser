from Dispenser import Dispenser

'''
Adds 'a', 'b', and 'c' randomly to string.
But stops 'c' from being dispensed after the first 'c' is dispensed.
'''
letters = Dispenser(objects=('a', 'b', 'c'), counts=(2, 3, 4), random_order=True)
string = ''
for L in letters:
  string += L
  if L == 'c':
    letters.disallow('c')
