# Dispenser
Smart object dispenser. In order or random order dispensing. Selective dispensing.  
✮✮✮✮✮✮✮✮✮✮✮✮ LIKE IT? STAR IT! ✮✮✮✮✮✮✮✮✮✮✮✮  
![](https://github.com/BebeSparkelSparkel/Dispenser/blob/master/dispenser.jpg?raw=true)

### Example
Adds 'a', 'b', and 'c' randomly to string.  
But stops 'c' from being dispensed after the first 'c' is dispensed.
```python
letters = Dispenser(objects=('a', 'b', 'c'), counts=(2, 3, 4), random_order=True)
string = ''
for L in letters:
  string += L
  if L == 'c':
    letters.disallow('c')
```

### Install
```shell
pip install git+git://github.com/BebeSparkelSparkel/Dispenser.git@master
pip3 install git+git://github.com/BebeSparkelSparkel/Dispenser.git@master
pip3.5 install git+git://github.com/BebeSparkelSparkel/Dispenser.git@master
```
### Initialize Dispenser
**Dispenser**(objects, counts, random_order=True)
```python
letters = Dispenser(('a', 'b', 'c'), (1, 2, 3))  # random order
tuple(letter)  # ('b', 'c', 'b', 'c', 'c', 'a')

letters = Dispenser(('a', 'b', 'c'), (1, 2, 3), random_order=False)  # in order
tuple(letter)  # ('a', 'b', 'b', 'c', 'c', 'c')
```
### Class Methods
**put_back**(self, obj)  
Adds one to the obj count.  
  
**disallow**(self, obj)  
No longer able to output obj until allowed again. 

**disallow_and_put_back**(self, obj)  
Adds one to the obj count.
No longer able to get obj from __next__ until allowed agian.

**allow_all**(self)  
Allows all objects to be distributed again.

**is_empty**(self)  
Returns True if all counts are zero.

**get_count**(self, obj)  
Returns the number left of obj in the dispenser.

**decrement**(self, obj, amount=1)  
Decreases the count of obj by amount.

✮✮✮✮✮✮✮✮✮✮✮✮ LIKE IT? STAR IT! ✮✮✮✮✮✮✮✮✮✮✮✮
