from setuptools import setup

setup(name='Dispenser',
      version='0.0.0',
      license='MIT',
      description='Smart object dispenser. In order or random order dispensing. Selective dispensing.',
      author='William Rusnack',
      author_email='williamrusnack@gmail.com',
      url='https://github.com/BebeSparkelSparkel/Dispenser',
      classifiers=['Development Status :: 2 - Pre-Alpha', 'Programming Language :: Python :: 3'],
      py_modules=['Dispenser'],
      install_requires=['orderedset==2.0']
     )
