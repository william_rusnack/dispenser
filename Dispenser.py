from collections import OrderedDict, Counter as col_Counter
from orderedset import OrderedSet
from itertools import accumulate
from random import randint


class Dispenser:
  def __init__(self, objects, counts, selector=None):
    '''
    objects - the objects that will be dispensed
    counts - the number of times an objext with the corresponding index can be dispensed
    selector - a function that determines which part should be dispensed
      defaults to Dispenser.selector_random
      other pre-defined methods are Dispenser.selector_ordered and Dispenser.selector_start_at()
      is passed the allowd object counts and must return the index of the count to be dispensed
    '''

    # left is the number left to be dispensed
    # disp is the number that has been dispensed
    self._objects_counts = OrderedDict(
        (obj, dotdict(left=c, disp=0))
        for obj, c in zip(objects, counts)
      )

    # objects in this can be selected
    self.allowed = OrderedSet(self._objects_counts.keys())

    # assigns the method of selecting the next object to dispense
    self.selector = selector if selector is not None else self.selector_random

  def put_back(self, obj):
    '''
    puts back one obj into the count
    adds one to the obj count
    '''
    if self._objects_counts[obj].disp - 1 < 0:
      raise ValueError('No {} has been dispensed yet'.format(obj))

    self._objects_counts[obj].left += 1
    self._objects_counts[obj].disp -= 1

  def disallow(self, obj):
    '''
    disallow the dispensement of obj
    to allow obj to be dispensed again the method allow must be called with obj as its argument
    '''
    self.allowed.remove(obj)

  def disallow_and_put_back(self, obj):
    '''
    puts back one obj into the count
    disallow the dispensement of obj
    '''
    self.disallow(obj)
    self.put_back(obj)

  def allow_all(self):
    '''
    allows all objects to be distributed again
    '''
    self.allowed = OrderedSet(self._objects_counts.keys())

  def not_empty(self):
    '''
    returns False if all counts are zero else True
    '''
    return any(c.left for c in self._objects_counts.values())

  def is_empty(self):
    '''
    returns True if all counts are zero else False
    '''
    return not self.not_empty()

  def get_count(self, obj):
    '''
    returns the number left of obj in the dispenser
    '''
    return self._objects_counts[obj].left

  def get_dispensed(self, obj):
    '''
    the number of obj that has been dispensed
    '''
    return self._objects_counts[obj].disp

  def decrement(self, obj, amount=1):
    '''
    decreases the count of obj by amount
    '''
    if self._objects_counts[obj].left - amount < 0:
      raise ValueError('Causes count to be less than zero.')

    self._objects_counts[obj].left -= amount
    self._objects_counts[obj].disp += amount

  def objects(self):
    '''
    Returns the objects that can be returned.
    Similar to dict.keys.
    '''
    return self._objects_counts.keys()

  @staticmethod
  def selector_random(counts):
    '''
    returns a random index
    '''
    try:
      rand_select = randint(1, sum(counts))
    except ValueError:
      raise StopIteration
    return sum(1 for current_total in accumulate(counts) if current_total < rand_select)

  @staticmethod
  def selector_ordered(counts):
    '''
    returns the first non-zero index
    '''
    return next(i for i in range(len(counts)) if counts[i] > 0)

  @staticmethod
  def selector_start_at(object_index):
    '''
    similar to selector_ordered but will start dispensing in order starting from object_index
    this is a function generator and must be called to get the selector function
    '''
    def selector(counts):
      for i_offset, count in enumerate(counts[object_index:]):
        if count > 0:
          return object_index + i_offset
      return Dispenser.selector_ordered(counts)

    return selector

  def __iter__(self):
    return self

  def __next__(self):
    '''
    returns the object selected by the selector function passed to __init__
    and decrements the object's count by 1
    '''
    obj = self.allowed[self.selector(tuple(
      self._objects_counts[obj].left for obj in self.allowed
    ))]
    self._objects_counts[obj].left -= 1
    self._objects_counts[obj].disp += 1
    return obj

  def __str__(self):
    return 'Dispenser ' + str(self._objects_counts)


class Counter(col_Counter):
  def add(self, obj):
    # try: self._counts[obj] += 1
    # except KeyError: self._counts[obj] = 1
    self.update((obj,))

  def get_count(self, obj):
    return self[obj]

  def objs(self):
    return self.keys()


class dotdict(dict):
  '''
  dictionary that works with dot notation
  http://stackoverflow.com/questions/2352181/how-to-use-a-dot-to-access-members-of-dictionary
  '''
  __getattr__ = dict.get
  __setattr__ = dict.__setitem__
  __delattr__ = dict.__delitem__
