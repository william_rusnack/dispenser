import unittest
from collections import Counter as col_Counter
from functools import reduce

from Dispenser import Dispenser, Counter



objects = 'a', 'b', 'c'
counts = 1, 2, 3
output_ordered = 'a', 'b', 'b', 'c', 'c', 'c'


class TestDispenser(unittest.TestCase):
  def test___init__(self):
    # empty
    dis = Dispenser((), ())
    self.assertTrue(dis.is_empty())
    with self.assertRaises(StopIteration): next(dis)

    # use case
    dis = Dispenser(objects, counts, Dispenser.selector_ordered)
    self.assertFalse(dis.is_empty())
    self.assertEqual(tuple(dis), output_ordered)

    # make sure dispenses randomly
    for i in range(10):
      dis_tuple = tuple(Dispenser(objects, counts))
      if dis_tuple != output_ordered: break
    self.assertNotEqual(dis_tuple, output_ordered)

  def test_put_back_and_decrement(self):
    # decrement
    dis = Dispenser(objects, counts)
    dis.decrement(objects[0])
    self.assertEqual(dis.get_count(objects[0]), counts[0] - 1)
    self.assertEqual(dis.get_dispensed(objects[0]), 1)

    dis.decrement(objects[1], 2)
    self.assertEqual(dis.get_count(objects[1]), counts[1] - 2)
    self.assertEqual(dis.get_dispensed(objects[1]), 2)

    with self.assertRaises(ValueError):
      Dispenser(['a'], [0]).decrement('a')

    # put_back
    with self.assertRaises(ValueError):
      dis.put_back(objects[2])

    dis.put_back(objects[0])
    self.assertEqual(dis.get_count(objects[0]), counts[0])
    self.assertEqual(dis.get_dispensed(objects[0]), 0)

  def test_allow(self):
    # disallow all
    dis = Dispenser(objects, counts, Dispenser.selector_ordered)
    for o in objects: dis.disallow(o)

    with self.assertRaises(StopIteration):
      next(dis)

    dis.allow_all()
    self.assertEqual(tuple(dis), output_ordered)

    # disallow one
    dis = Dispenser(objects, counts, Dispenser.selector_ordered)
    dis.disallow(objects[-1])
    self.assertEqual(
        tuple(dis),
        tuple(obj for obj in output_ordered if obj != objects[-1])
      )
    dis.allow_all()
    self.assertEqual(
        tuple(dis),
        tuple(obj for obj in output_ordered if obj == objects[-1])
      )

  def test_is_empty(self):
    self.assertTrue(Dispenser((), ()).is_empty())
    self.assertFalse(Dispenser(objects, counts).is_empty())

    dis = Dispenser(objects, counts)
    for obj in dis: pass
    self.assertTrue(dis.is_empty())

  def test_get_count(self):
    with self.assertRaises(KeyError):
      Dispenser((), ()).get_count('a')

    self.assertEqual(Dispenser(objects, counts).get_count('a'), 1)

  def test_get_dispensed(self):
    with self.assertRaises(KeyError):
      Dispenser((), ()).get_count('a')

    self.assertEqual(Dispenser(objects, counts).get_dispensed('a'), 0)

  def test_objects(self):
    dis = Dispenser(objects, counts)
    self.assertEqual(tuple(dis.objects()), objects)


class TestSelectors(unittest.TestCase):
  def test_selector_random(self):
    selector_random = Dispenser.selector_random

    with self.assertRaises(StopIteration): selector_random(())
    with self.assertRaises(StopIteration): selector_random((0, 0))

    self.assertEqual(selector_random((1,)), 0)
    self.assertTrue(len(set(selector_random((1,2,3)) for i in range(50))) > 1)

  def test_selector_ordered(self):
    selector_ordered = Dispenser.selector_ordered
    with self.assertRaises(StopIteration): selector_ordered(())
    with self.assertRaises(StopIteration): selector_ordered((0, 0))

    self.assertEqual(selector_ordered((1, 1)), 0)
    self.assertEqual(selector_ordered((0, 2)), 1)

  def test_selector_start_at(self):
    selector_start_at = Dispenser.selector_start_at

    selector = selector_start_at(0)
    with self.assertRaises(StopIteration): selector(())
    with self.assertRaises(StopIteration): selector((0, 0))
    self.assertEqual(selector((1, 1)), 0)
    self.assertEqual(selector((0, 2)), 1)

    selector = selector_start_at(1)
    self.assertEqual(selector((1, 1)), 1)
    self.assertEqual(selector((0, 2)), 1)
    self.assertEqual(selector((2, 0)), 0)


class TestCounter(unittest.TestCase):
  def test(self):
    c = Counter()
    c.add('a')
    self.assertEqual(c.get_count('a'), 1)
    c.add('a')
    self.assertEqual(c.get_count('a'), 2)
    c.add('b')
    self.assertEqual(c.get_count('b'), 1)

    self.assertEqual(set(c.objs()), {'a', 'b'})


if __name__ == '__main__':
    unittest.main()
